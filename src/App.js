import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Academies from './components/Academies';
import Footer from './components/Footer';
import Event_space from './components/Event_space'
import Error from './components/Error';

function App() {
  return (
    <BrowserRouter>
        {/* <div className='App'> */}
          <Navbar/>
          <Switch>
           <Route exact path='/' component={Academies}/>
           <Route path="/Event_space.html" component={Event_space}/>
           <Route component={Error} />
          </Switch>
          <Footer/>
        {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
