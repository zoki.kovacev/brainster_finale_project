import React from 'react';
import Logo from '../brainster_space_logo.svg'
const Footer = () => {
    return (
        <div className="Footer">
            <div className="container-fluid footerFirstRow">
                <div className="row">
                    <div class="col-md-2 col-md-offset-1 padding10">
                        <p className="font-weight-bold paddingBottom10">Корисни линкови</p>
                        <p className="cursorPointer" data-toggle="" >Контакт</p>
                        <a className="colorInherit" href="https://www.wearelaika.com/"><p>Отворени позиции</p></a>
                        <a className="colorInherit" href="https://medium.com/wearelaika/brainster-space-the-"><p>Галерија</p></a>
                        <a className="colorInherit" href="event.html"><p>Календар</p></a>
                    </div>
                    <div className="col-md-2 padding10">
                        <p className="font-weight-bold paddingBandL10">Социјални мрежи</p>

                        <a href="https://www.facebook.com/brainster.co" target="_blank" rel="noopener noreferrer">
                            <i className="fa fa-facebook fb"></i>
                        </a>
                        <a href="https://www.linkedin.com/school/brainster-co/" target="_blank" rel="noopener noreferrer">
                            <i className="fa fa-linkedin linked"></i>
                        </a>
                        <a href="https://www.instagram.com/brainsterco/" target="_blank" rel="noopener noreferrer">
                            <i className="fa fa-instagram instagram"></i>
                        </a>
                    </div>
                    <div className="col-md-2 col-md-offset-5 padding10">
                        <a className="navbar-brand" href="/"><img src={Logo} alt=""></img></a>
                    </div>
                </div>
            </div>
            <div className="footer">
                <p className="blockMargin">COPYRIGHT©BrainsterSpace. AllRights Reserved</p>
            </div>
        </div>
    );
};

export default Footer;
