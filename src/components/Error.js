import React from 'react'
import { Link } from 'react-router-dom';

const Error = () => {
  return <div className='ErrorPage text-center'>
    <h1>Error <span>404</span> </h1>
    <p>You reached dead end. GO back to the <Link to='/'>Homepage</Link> </p>
  </div>;
}

export default Error