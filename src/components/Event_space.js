import React from 'react';

const Event_space = (props) => {
    return (
        <div className="Event_space">

        {/* *******************************Prostor za nastani********************* */}
            <div className="container martingTop20">
                <div className="row verticalAlign">

                    <div className="col-md-12 col-lg-4">
                        <h1 className="font-weight-bold fontSize35em">Простор за настани</h1>
                        <p>Нашиот простор се прилагодува според потребите на вашиот настан. Седум различни простории и Space
                        Kitchen.
                            <br />
                            <br />
                            Наменски создадени да се прилагодуваат и менуваат во согласност со типот на настан кој го
                            органицирате.
                            <br />
                            <br />
                            Организираме конференции до 150 учесници и обуки и предавања за групи од 20 учесници. Контактирај не
                            за да ви хостираме одличен настан.

                        </p>
                        <a href="#evenHost"><button className="btn danger1" id="educationBtn">→ &nbsp;БУКИРАЈ НЕ</button></a>

                    </div>

                    <div className="col-md-12 col-lg-8">
                        <img src={require("../img/Za_Nas/prostor_za_nastani.jpg")} className="img-responsive" alt="..." />
                    </div>
                </div>
            </div>
             {/* *******************************Prostor za nastani-END********************* */}
             {/* *******************************Nasite prostorii********************* */}
            <div className="container marginTop70">
                <div className="row verticalAlign">
                    <div className="col-md-12 col-lg-3">
                        <h1 className="font-weight-bold fontSize35em">Нашите простории</h1>
                        <p>Комплетно адаптибилни. Една сала за 150 учесници или три помали сали за групи од по 50 учесници.
                        Училници за од 25 до 40 учесника. Избор од две локации.
                            <br />
                            <br />
                        Пулт за прием. И најважното место за networking - Brainster Kitchen.
                            <br />
                            <br />
                        Како го замислувате вашиот следен настан?
                        </p>
                    </div>
                    <div className="col-md-12 col-lg-9">
                        <div className="row">
                            <div className="col-lg-4 col-md-6">
                                <img src={require("../img/Event_space/2.jpg")} className="img-event-space" alt="..." />
                                <p className="bold">Brainster</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/C3 EXIBITION_1.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Конференциска сала</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/C3 2.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Сала со бина</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/C3 1.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p class="bold">Адаптибилна училница</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/C2 1.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Училница</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/C1_1.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Училница</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/HOL KON SEDENJE.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Хол</p>
                            </div>
                            <div className="col-lg-4 col-md-6">

                                <img src={require("../img/Event_space/C1_4.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Канцелариски простор</p>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <img src={require("../img/KITCHEN_03.jpg")} className="img-responsive img-event-space" alt="..." />
                                <p className="bold">Space Kitchen</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {/* *******************************Nasite prostorii-END********************* */}
            {/* *******************************Space-kitchen********************* */}
            <div className="container marginTop70">
                <div className="row verticalAlign">
                    <div className="col-lg-8">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <img
                                            src={require("../img/Space_Kitchen_Galerija/IMG_7777.jpg")}
                                            className="img1_kitchen img-responsive"
                                            alt = "..."
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <img
                                            src={require("../img/Space_Kitchen_Galerija/IMG_7385.jpg")}
                                            className="img2_kitchen img-responsive"
                                            alt = "..."
                                        />
                                    </div>
                                    <div className="col-lg-6">
                                        <img
                                            src={require("../img/Space_Kitchen_Galerija/IMG_7361.jpg")}
                                            className="img3_kitchen img-responsive"
                                            alt = "..."
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <img
                                    src={require("../img/Space_Kitchen_Galerija/IMG_7362.jpg")}
                                    className=" img4_kitchen img-responsive"
                                    alt = "..."
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 paddingRight20" id="EventSpaceMobile">
                        <h1 className="font-weight-bold">Space Kitchen</h1>
                        <p>
                            Место каде сите настани започнуваат и завршуваат. Место каде нашиот
                            тим готви и експериментира. Нашата кујна има само едно правило.
                            <br /> 
                            <br />
                             Храната мора да биде вегетаријанска. Пијалок, кафе или мезе. Вие
                            одберете вид на кетеринг ние ќе го обезбедиме.
                         </p>
                    </div>
                </div>
            </div>
            {/* *******************************Space-kitchen-END********************* */}
            <br></br>
            <br></br>
            {/* *******************************Nudime********************* */}
            <div className="container">
                <h1>Нудиме</h1>
                <div className="flex">
                    <p className="danger2">
                        <i className="fa fa-square colorfcd132">
                        </i> &nbsp;Простор</p>
                    <p className="danger2">
                        <i className="fa fa-pizza-slice colorfcd132">
                        </i> &nbsp;Space Kitchen</p>
                    <p className="danger2">
                        <i className="fa fa-globe colorfcd132">
                        </i>&nbsp;Логистика</p>
                    <p className="danger2">
                        <i className="fa fa-headset colorfcd132">
                        </i> &nbsp;Техничка подршка</p>
                    <p className="danger2">
                        <i className="fa fa-volume-up colorfcd132">
                        </i>&nbsp;Звук</p>
                    <p className="danger2">
                        <i className="fa fa-lightbulb colorfcd132">
                        </i>&nbsp;Светло</p>
                    <p className="danger2">
                        <i className="fa fa-network-wired colorfcd132">
                        </i>&nbsp;Помош при Организација</p>
                    <p className="danger2">
                        <i className="fa fa-film colorfcd132">
                        </i> &nbsp;Видео и Фотографија</p>
                    <p className="danger2">
                        <i className="fa fa-facebook colorfcd132">
                        </i>&nbsp;Промоција на Социјални Мрежи</p>
                </div>
            </div>
            {/* *******************************Nudime-END********************* */}
            {/* *******************************Event Host********************* */}
            <div className="container" id="evenHost">
                <div className="row verticalAlign proba">
                    <div className="col-md-6 col-lg-7">
                        <h1 className="font-weight-bold fontSize35em">Event Host</h1>
                        <p>Ања Макеска
                            <br />
                            <br />
                            anja@brainster.co
                            <br />
                            <br />
                            +389 (0)70 233 414
                            </p>
                    </div>
                    <div className="col-md-6 col-lg-5">
                        <img src={require("../img/eventHost.jpg")} className="img-responsive" alt="..." />
                    </div>
                </div>
            </div>
            <br />
            <br />
            {/* *******************************Event Host-END********************* */}

        </div>

    );
};

export default Event_space;
