import React, { Component } from 'react';


class Card extends Component {
    constructor() {
        super();
        this.state = {};
    }
    render() {
        return (
            <div class="card">
            <div className="content">
                <div className="col-md-6 col-lg-4">
                    <div className="thumbnail box">
                        <a href={this.props.href} target="_blank" rel='noopener noreferrer'></a>
                        <img className="img-responsive" src={this.props.image} alt="" />
                        <div className="caption">
                            <h4 className="font-weight-bold">{this.props.title}</h4>
                            <p className="myP">{this.props.body}</p>
                            <p className="textAlignRightFont30">
                                <i href="#" className="fa fa-arrow-circle-o-right" aria-hidden="true" />
                            </p>
                            <p>{this.props.space}</p>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
};
export default Card;
