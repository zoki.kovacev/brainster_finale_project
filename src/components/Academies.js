import React from 'react';
import { Component } from 'react';
import Card from './Card';




class Academies extends Component {
    constructor() {
        super();
        this.state = {
            toggle: false,
        }   

        this.education = {
            url: "Za_Nas/edukacija1",
            title: "Едукација",
            text: `
                    Дали си подготвен да одговориш на потребите на иднината. Вистинските курсеви, академии и семинари кои ќe ти овозможат кариерна трансформација во областа дигитален маркетинг, дизајн, програмирање и Data Sience.
                    `,
        };
        this.academy = {
            url: "IMG_7707",
            title: "Компании",
            text: `Копманиите имаат можност да ги надоградат своите тимови, а со тоа да го подобрат перформансот на својата компанија.\n
            Дигиталната трансформација се случува, а вашите компании треба да бидат подготвени за да се адаптираат соодветно. Обуки, семинари, курсеви или team building?\n
            Во Brainster Space имаме специјално обучен тим кој е подготвен да ја ннасочи адаптира сподели својата експертиза со денешните потреби на компаниите.
            `,
        };
        this.state = this.education;
    }

    toggleImage = (type) => {
        this.setState((state) =>
            type === "edukacija" ? this.education : this.academy
        );
    };


    toggleHandler = (e) => {
        this.setState({
            toggle: !this.state.toggle,
        });
        const text = e.target;
        text.style.textDecoration === 'line-through'
            ? (text.style.textDecoration = 'none')
            : (text.style.textDecoration = 'line-through');
    };




    render() {
        return (
            <div className="Academies">
                <div className="bg-image">
                    <div className="container-floid opacity">
                        <h1 className="top">Центар за Учење, Кариера и <br /> Иновации </h1>
                    </div>
                </div>
                {/* ***************************Za nas********************************** */}
                <div className="forUs">
                    <h2>За нас</h2>
                </div>
                <div className="container">
                    <div className="row">

                        <a href="https://brainster.co/" target="_blank" rel="noopener noreferrer">
                            <Card
                                // href="https://brainster.co/"
                                image={require('../img/Za_Nas/edukacija1.jpg')}
                                title="Едукација"
                                body="Биди дел од tech заедницата на иноватори, креативци и претприемачи. Резервирај стол во нашата shared
                                    office. Пичирај го твојот бизнис и нашиот тим заедно ќе одлучи секој месец со кого да ги дели своите
                                канцеларии.
                                "
                            />
                        </a>

                        <a href="event.html" target="_blank" rel="noopener noreferrer">
                            <Card
                                image={require("../img/Za_Nas/edukacija2.jpg")}
                                title="Настани"
                                body="Специјално курураби и организирани настани кои ги поврзуваат правите таленти со иновативните компании.
                                    Идејата е да нашата tech заедница расте, се инспирира и креира преку овие настани."
                                href="event.html"

                            />
                        </a>

                        <Card
                            image={require("../img/Za_Nas/coworking.jpg")}
                            title="Coworking"
                            body="Биди дел од tech заедницата на иноватори, креативци и претприемачи. Резервирај стол во нашата shared
                                    office. Пичирај го твојот бизнис и нашиот тим заедно ќе одлучи секој месец со кого да ги дели своите
                                канцеларии.
                                "
                        />

                        <a href="Event_space.html" target="_blank" rel="noopener noreferrer">
                            <Card
                                image={require("../img/Za_Nas/prostor_za_nastani.jpg")}
                                title="Простор за настани"
                                body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                                        најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                                href="Event_space.html"
                            />
                        </a>

                        <a href="https://drive.google.com/drive/u/0/folders/1uLpVURtS6fa0TODdFtIO3YWVo-eHXo77" target="_blank" rel="noopener noreferrer">
                            <Card
                                image={require("../img/partnerstva so tech komp.jpg")}
                                title="Партнерства со Tech компании"
                                body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                                href="https://drive.google.com/drive/u/0/folders/1uLpVURtS6fa0TODdFtIO3YWVo-eHXo77"

                            />
                        </a>
                        <a href data-toggle='modal' data-target="Modal" rel="noopener noreferrer">
                        <Card
                            image={require("../img/IMG_7397.jpg")}
                            title="Иновации за компании"
                            body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                            

                        />
                        </a>
                    </div>
                </div>
                <br />
                <br />
                {/* ***************************Za nas-END********************************** */}
                {/* ****************************Educattion******************************** */}

                <section id="education">
                    <div className="container">
                        <div className="row verticalAlign ">
                            <div className="col-lg-5 col-md-12">
                                <h1>{this.state.title}</h1>
                                <p>{this.state.text}</p>
                                <button
                                    className="btn danger1"
                                    id="educationBtn1"
                                    onClick={() => this.toggleImage("edukacija")}
                                >
                                    АКАДЕМИИ
                                </button>
                                <button
                                    className="btn danger1 danger3"
                                    id="companyBtn1"
                                    onClick={() => this.toggleImage("kompanii")}
                                >
                                    ЗА КОМПАНИИ
                                </button>
                            </div>
                            <div className="col-lg-7 col-md-12">
                                <img
                                    src={require(`../img/${this.state.url}.jpg`)}
                                    className="borderRadius10 img-responsive slider"
                                    alt="..."
                                />
                            </div>
                        </div>
                    </div>
                </section>
            {/* ****************************Educattion-END******************************** */}

            {/* **************************Nastani******************************* */}
            
                <div className="forUs">
                    <h2>Настани</h2>
                </div>
                <div className="container">
                    <div className="row">

                        <Card 
                            image={require("../img/Nastani/IMG_7481.jpg")}
                            title="Иновации за компании"
                            body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                        />
                        <Card
                            image={require("../img/Nastani/instruktori.jpg")}
                            title="Иновации за компании"
                            body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                        />
                        <Card
                            image={require("../img/Nastani/Hristijan-Nosecka-1024x536.jpg")}
                            title="Иновации за компании"
                            body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                                    персонализирани обуки кои одговараат на реалните потреви на денешницата."

                        />
                    </div>
                    </div>
                    
                    <div className="container ">
                        <div className="BtnCalendar">
                            <a href="event.html" target="_blank">
                                <button className="btn danger1"><i class="fa fa-calendar"></i> &nbsp; КАЛЕНДАР НА НАСТАНИ</button>
                            </a>
                        </div>
                    </div>

                    {/* **************************Nastani-END******************************* */}
                    {/* *************************Coworking********************************8 */}
                    <div className="container ">
                        <div className="row verticalAlign">
                            <div className="col-lg-7 col-md-12 desktopshow" id="coWorking">
                                <img src={require("../img/Za_Nas/coworking.jpg")} className="borderRadius10 img-responsive" alt="..."></img>
                            </div>

                            <div className="col-lg-5 col-md-12">
                                <span className="badge">SOLD OUT</span>
                                <h1 className="textDecorLine-Th">Coworking</h1>
                                <p className="textDecorLine-Th">Биди дел од tech заедницата на иноватори, креативци и претприемачи.
                                Резервирај стол во нашата share offcie.
                                    Пичирај го твојот бизнис и нашиот тим заедно ќе одлучи секој месец со кого да ги дели своите канцеларии.</p>
                                <button className="btn danger1 textDecorLine-Th" onClick={this.toggleHandler}>РЕЗЕРВИРАЈ МЕСТО</button>
                                {this.state.toggle ?
                                    <div id="alert" className="alert alert-danger" role="alert">Местата се распродадени.</div>
                                    : null}
                            </div>
                        </div>
                    </div>
                    {/* *************************Coworking-END******************************** */}
                    {/* *************************Prostor za nastani*************************** */}
                        <div className="container padding-top-xl">
                            <div className="row verticalAlign">
                                <div className="col-lg-5 col-md-12">
                                    <h1>Простор за настани</h1>
                                    <p>Можност за презентации, обуки, конференции, netwoking настани. Одбери ја просторијата која најмногу ќе
                                    одговара на твојата идеја. Го задржуваме правото да одбереме кој настан ќе се организира во нашиот Brainster
                                        Space.</p>
                                    <a href="Event_Space.html" target="_blank" rel="noopener">
                                        <button className="btn danger1" id="educationBtn">→ &nbsp;ВИДИ ГО ПРОСТОРОТ</button>
                                    </a>
                                </div>
                                <div className="col-lg-7 col-md-12">
                                    <br />
                                    <br />
                                    <img src={require("../img/Za_Nas/prostor_za_nastani.jpg")} className="borderRadius10 img-responsive" alt="..."></img>
                                </div>
                            </div>
                        </div>
                        {/* *************************Prostor za nastani-END*************************** */}
                        {/* *************************Partneri*************************** */}
                        <div className="forUs">
                            <h1>Партнери</h1>
                            <h3>Имаш идеја? Отворени сме за соработка</h3>
                            <a href="Event_Space.html" target="_blank" rel="noopener">
                                <button className="btn danger1" id="educationBtn">→ &nbsp;ВИДИ ГО ПРОСТОРОТ</button>
                            </a>

                        </div>
                    {/* *************************Partneri*************************** */}


                

            </div>
        );
    };
}
export default Academies;
