import React from 'react';
import Modal from './Modal.js';
import useForm from './useForm';
import validate from './validateInfo';

// const BUTTON_WRAPPER_STYLERS = {
//     position: 'relative',
//     zIndex: 1,
//     color: 'orange',
//     border: '3px solid black',
//     backgroundColor: 'black',
//     padding: '10px',
//     borderRadius: '5px',
//     marginLeft: '200px',
// }

const INPUT_STYLE ={
    width:'400px',
    height:'30px',
    fontStyle: 'italic'

}

const LABEL_STYLE = {
    fontWeight: 'bold',
    marginBottom: '10px'

}

const TEXTAREA_STYLE = {
    width:'400px',
    height:'100px',
    fontStyle: 'italic'
}

const BUTTON_CLOSE_STYLE ={
    border:'none',
    backgroundColor:'white',
    fontWeight:'bold',
    float:'right',
    marginTop:'15px',
    borderRadius:"none"
}


const BUTTON_JOIN_STYLE = {
    color: 'orange',
    border: '3px solid black',
    backgroundColor: 'black',
    padding: '10px',
    float:'right',
    marginLeft: '10px'
}




const Join_Us =(submitFrom) => {
    const [isOpen, setIsOpen] = React.useState(false)

    const { handleChange, values, handleSubmit, errors } = useForm(submitFrom,validate);
    
    
    return (
        <div>
            <button className="btn danger" id="BtnTablet"  onClick={() => setIsOpen(true)}>+ &nbsp; ПРИКЛУЧИ СЕ</button>
            <Modal open={isOpen} onClose={() => setIsOpen(false)}>
                <form className="form-content" onSubmit={handleSubmit}>
                    <h4 class="modal-title font-weight-bold">Приклучи се</h4>
                    <button  className="close" onClick={()=>setIsOpen(false)}>X</button>
                    <br/>
                    <hr/>
                    <label htmlFor="username" style={LABEL_STYLE}>Име и презиме (задолжително)</label>
                    <br />
                    <input type="text" id="username" placeholder="Внесете Име и Презиме" name="username" required value={values.username} onChange={handleChange} style={INPUT_STYLE}></input>
                    {errors.username && <p>{errors.username}</p>}
                    <br />
                    <br />
                    <label htmlFor="NameCompany" style={LABEL_STYLE}>Име на компанија (незадолжително)</label>
                    <br />
                    <input type="text" id="NameCompany" placeholder="Внесете Име на Компанија" name="NameCompany" style={INPUT_STYLE}></input>
                    <br />
                    <br />
                    <label htmlFor="phone" style={LABEL_STYLE}>Телефонски број (задолжително)</label>
                    <br />
                    <input type="text" id="phone" placeholder="Внесете Телефонски Број" name="phone" required value={values.phone} onChange={handleChange} style={INPUT_STYLE}></input>
                    {errors.phone && <p>{errors.phone}</p>}
                    <br />
                    <br />
                    <label htmlFor="PCorporation" style={LABEL_STYLE}>Предлог за соработка (незадолжително)</label>
                    <br />
                    <textarea id="PCorporation" placeholder="Во 300 карактери, опишете зошто сакате да соработуваме" style={TEXTAREA_STYLE}></textarea>
                    <br />
                    <br />
                    <hr/>
                    <input  type='submit' style={BUTTON_JOIN_STYLE} className="from input btn btn_join" value=' → ИСПРАТИ ФОРМА'/>
                    <input  type='button' onClick={()=>setIsOpen(false)} style={BUTTON_CLOSE_STYLE}  value='ИСКЛУЧИ'/>
                </form>
            </Modal>
        </div>

    )

}
export default Join_Us;
