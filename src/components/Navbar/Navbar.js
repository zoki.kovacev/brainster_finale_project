import React, { useState } from 'react';
import { Button } from './Button';
import { Link } from 'react-router-dom';
import './Navbar.css';
import Dropdown from './Dropdown';
import Logo from '../../brainster_space_logo.svg'

function Navbar() {
  const [click, setClick] = useState(false);
  const [dropdown, setDropdown] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const onMouseEnter = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(true);
    }
  };

  const onMouseLeave = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(false);
    }
  };

  return (
    <>
      <nav className='navbar'>
        <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
							<img
								src={Logo}
								alt="Logo"
							/>
        </Link>
        <div className='menu-icon' onClick={handleClick}>
          <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
        </div>
        <ul className={click ? 'nav-menu active' : 'nav-menu'}>
          <li className='nav-item'>
            <Link to='/event.html' className='nav-links' onClick={closeMobileMenu}>
              Настани
            </Link>
          </li>
          <li
            className='nav-item'
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
          >
            <Link
              to='/co-working'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              Co-workink
            </Link>
            {dropdown && <Dropdown />}
          </li>
          <li className='nav-item'>
            <Link
              to='/'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              Академии
            </Link>
          </li>
          <li className='nav-item'>
            <Link
              to="/Event_space.html"
              className='nav-links'
              onClick={closeMobileMenu}
            >
              Простор за настани
            </Link>
          </li>
          <li className='nav-item'>
            <Link
              to="/friends-ship"
              className='nav-links'
              onClick={closeMobileMenu}
            >
              Партнерства
            </Link>
          </li>
        </ul>
        <div className='nav-item'>
        <Button />
        </div>
        
      </nav>
    </>
  );
}

export default Navbar;